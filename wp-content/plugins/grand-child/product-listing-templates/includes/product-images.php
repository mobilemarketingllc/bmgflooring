<?php
        $collection = get_field('collection');
?>
<div id="product-images-holder"  <?php if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { ?>class="colorwall-exlusive-batch"<?php } ?>>
    <?php
        $image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 

        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
                $image = "http://" . $image;
            }	
            $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
            $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
        }
        else{
            if(strpos($image , 'http') === false){ 
                $image = "https://" . $image;
            }	
            $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[150]&sink";
            $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
        }

        if (!empty($image)){
    ?>
    <div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: 100% 100%;background-position:center" data-targetimg="gallery_item_0" data-responsive="<?php echo $image; ?>" data-src="<?php echo $image; ?>" data-exthumbimage="<?php echo $image_thumb; ?>">
        <a href="javascript:void(0)" class="popup-overlay-link"></a>
        <span class="main-imgs"><img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } else{ ?>
    <div class="img-responsive toggle-image" data-targetimg="gallery_item_0" style="background-image:url('http://placehold.it/168x123?text=COMING+SOON');background-size: cover;background-position:center">
        <a href="http://placehold.it/168x123?text=COMING+SOON" class="popup-overlay-link"></a>
        <span class="main-imgs"><img src="http://placehold.it/168x123?text=COMING+SOON" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" /></span>
    </div>
    <?php } ?>
    <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gal_count = 1;
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {
                $room_image = $value;
            
                if(strpos($room_image , 's7.shawimg.com') !== false){
                    if(strpos($room_image , 'http') === false){ 
                        $room_image = "http://" . $room_image;
                    }
                    $room_image_thumb = $room_image;
                    $room_image = $room_image;
                } else{
                    if(strpos($room_image , 'http') === false){ 
                        $room_image = "https://" . $room_image;
                    }
                    $room_image_thumb= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[150]&sink";
                    $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[1200]&sink";
                }
        ?>
            <div class="popup-imgs-holder" data-targetimg="gallery_item_<?php echo $gal_count; ?>" data-responsive="<?php echo $room_image; ?>" data-exthumbimage="<?php echo $room_image_thumb; ?>" data-src="<?php echo $room_image; ?>"><a href="javascript:void(0)" title="<?php the_title_attribute(); ?>"><span class="main-imgs"><img src="<?php echo $room_image; ?>" alt="<?php the_title_attribute(); ?>" /></span></a></div>
        <?php
            $gal_count++;
            }
        }
        ?>
</div>
    <?php if(get_field('gallery_room_images')){ ?>
    <div class="toggle-image-thumbnails <?php if($LAYOUT_COL == 5) : echo "vertical-slider"; endif; ?>">
        <?php if (!empty($image)){ ?>
            <div class="toggle-image-holder"><a href="javascript:void(0)" class="active" data-targetimg="gallery_item_0" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
        <?php } ?>       
        <?php
        // check if the repeater field has rows of data
        if(get_field('gallery_room_images')){
            $gallery_count = 1;
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {
                $room_image = $value;
            
                if(strpos($room_image , 's7.shawimg.com') !== false){
                    if(strpos($room_image , 'http') === false){ 
                        $room_image = "http://" . $room_image;
                    }
                    $room_image_small = $room_image ;
                    $room_image = $room_image ;
                } else{
                    if(strpos($room_image , 'http') === false){ 
                        $room_image = "https://" . $room_image;
                    }
                    $room_image_small= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[150]&sink";
                    $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[600x400]&sink";
                }
        ?>
            <div class="toggle-image-holder">
                <a href="javascript:void(0)" data-targetimg="gallery_item_<?php echo $gallery_count; ?>" data-background="<?php echo $room_image; ?>" data-thumbnail="<?php echo $room_image_small; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
            </div>
        <?php
            $gallery_count++;
            }
          }
        ?>
    </div>
<?php } ?>